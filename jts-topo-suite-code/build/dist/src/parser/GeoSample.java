  package parser;

  import com.vividsolutions.jts.algorithm.locate.IndexedPointInAreaLocator;
  import com.vividsolutions.jts.geom.Coordinate;
  import com.vividsolutions.jts.geom.Geometry;
  import com.vividsolutions.jts.geom.GeometryFactory;
  import com.vividsolutions.jts.geom.Polygon;

  import java.io.BufferedReader;
  import java.io.BufferedWriter;
  import java.io.File;
  import java.io.FileInputStream;
  import java.io.FileOutputStream;
  import java.io.InputStreamReader;
  import java.io.OutputStreamWriter;
  import java.nio.file.Path;
  import java.util.ArrayList;
  import java.util.HashSet;
  import java.util.List;
  import java.util.Random;
  import java.util.Set;

  /**
   * Created by ravi on 20/12/16.
   */
  public class GeoSample extends Thread{

    private static IndexedPointInAreaLocator locator;
    private static int GeofenceNo = 500000;
    private static int ValidIdSize = 100;
    private static int coordsSize = 200;
    private static int minYIndia = 12;
    private static int maxYIndia = 30;
    private static int minXIndia = 72;
    private static int maxXIndia = 88;

    private static Random random = new Random();

    public static void generateKML () throws Exception{
      for(int i=1;i<=5000;i++){

      File file = new File("KML/"+"map("+i+").kml");
      file.createNewFile();
      FileOutputStream stream = new FileOutputStream(file);
      BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(stream));
      bufferedWriter.write("<?xml version='1.0' encoding='UTF-8'?><kml xmlns='http://www.opengis.net/kml/2.2'><Document><Placemark><ExtendedData></ExtendedData><Polygon><outerBoundaryIs><LinearRing><coordinates>");

        int len = random.nextInt(20-15)+15;
        double randomX = (random.nextInt(maxXIndia-minXIndia)+minXIndia)+random.nextDouble();
        double randomY = random.nextInt(maxYIndia-minYIndia)+minYIndia+random.nextDouble();
        double deltaXHalf = random.nextDouble()/200;
        double deltaY = random.nextDouble()/100;
        double yi = (randomY-deltaY/2);
        bufferedWriter.write(randomX+","+yi+" ");

        int halfLen = len/2;
        int quaterLen = len/4;
        double y = yi;
        double x = randomX;
        for(int j=1;j<=halfLen;j++){
          y = y+(deltaY/halfLen);
          if(j<=quaterLen){
            x = x - (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+" ");
          }else{
            x = x + (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+" ");
          }
        }
        for(int k=1;k<=halfLen;k++){
          y = y-(deltaY/halfLen);
          if(k<=quaterLen){
            x = x + (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+" ");
          }else{
            x = x - (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+" ");
          }
        }
        bufferedWriter.write(randomX+","+yi);
        bufferedWriter.write("</coordinates> </LinearRing> </outerBoundaryIs> </Polygon> </Placemark> </Document> </kml>");
        bufferedWriter.newLine();
        bufferedWriter.flush();
        bufferedWriter.close();
        stream.close();
      }

    }
    public static void fillCoordinates() throws Exception{
      File file = new File("coords.txt");
      file.createNewFile();
      FileOutputStream stream = new FileOutputStream(file);
      BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(stream));
      for(int i=1;i<=coordsSize;i++){
        double randomX = (random.nextInt(maxXIndia-minXIndia)+minXIndia)+random.nextDouble();
        double randomY = random.nextInt(maxYIndia-minYIndia)+minYIndia+random.nextDouble();
        bufferedWriter.write(((random.nextDouble()/100)+randomX)+","+((random.nextDouble()/100)+randomY));
        bufferedWriter.newLine();
        bufferedWriter.flush();
      }
      bufferedWriter.close();
      stream.close();
    }
    public static void fillGeofence() throws Exception{
      File file = new File("geofence.txt");
      file.createNewFile();
      FileOutputStream stream = new FileOutputStream(file);
      BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(stream));
      for(int i=1;i<=1000000;i++){

        bufferedWriter.write(i+"#");
        int len = random.nextInt(20-15)+15;
        double randomX = (random.nextInt(maxXIndia-minXIndia)+minXIndia)+random.nextDouble();
        double randomY = random.nextInt(maxYIndia-minYIndia)+minYIndia+random.nextDouble();
        double deltaXHalf = random.nextDouble()/200;
        double deltaY = random.nextDouble()/100;
        double yi = (randomY-deltaY/2);
        bufferedWriter.write(randomX+","+yi+","+0.0+" ");

        int halfLen = len/2;
        int quaterLen = len/4;
        double y = yi;
        double x = randomX;
        for(int j=1;j<=halfLen;j++){
           y = y+(deltaY/halfLen);
          if(j<=quaterLen){
            x = x - (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+","+0.0+" ");
          }else{
            x = x + (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+","+0.0+" ");
          }
        }
        for(int k=1;k<=halfLen;k++){
          y = y-(deltaY/halfLen);
          if(k<=quaterLen){
            x = x + (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+","+0.0+" ");
          }else{
            x = x - (deltaXHalf/quaterLen);
            bufferedWriter.write(x+","+y+","+0.0+" ");
          }
        }
        bufferedWriter.write(randomX+","+yi+","+0.0);
          bufferedWriter.newLine();
        bufferedWriter.flush();
      }
      bufferedWriter.close();
      stream.close();
    }


    public static void main(String [] args) throws Exception{
      GeometryFactory factory = new GeometryFactory();
      List<Polygon> dev1FenceCollection = new ArrayList<>();
      List<Polygon> dev1FenceCollection1 = new ArrayList<>();
      Set<Long> validIds = new HashSet<>();
      Long start = System.currentTimeMillis();
      //fillGeofence();
      fillCoordinates();
       //generateKML();
      System.out.println("start reading");
      List<Double> arrX = new ArrayList<>();
      List<Double> arrY = new ArrayList<>();
      {
        File file = new File("coords.txt");
        FileInputStream inputStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        for (int i = 1; i <= coordsSize && (line = reader.readLine()) != null; i++) {
          String arr[] = line.split(",");
          arrX.add(Double.valueOf(arr[0]));
          arrY.add(Double.valueOf(arr[1]));
        }
        line = null;
        reader.close();
        inputStream.close();
      }
      {
        File file = new File("geofence.txt");
        FileInputStream inputStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        for (int i = 1; i <= GeofenceNo && (line = reader.readLine()) != null; i++) {
          String arr[] = line.split("#");
          dev1FenceCollection.add(NVTGeoParser.parsePolygon(factory, arr[1], Long.parseLong(arr[0])));
        }
        line = null;
        reader.close();
        inputStream.close();
      }
      for (int l=1;l<=ValidIdSize;l++){
        validIds.add(Long.parseLong(l+""));
      }
      //dev1FenceCollection.add(NVTGeoParser.parsePolygon(factory, "0,0,0 2,0,0 2,2,0 0,2,0 0,0,0", 1L));
      //dev1FenceCollection.add(NVTGeoParser.parsePolygon(factory, "1,3,0 4,3,0 4,6,0 1,6,0 1,3,0", 2L));

      //validIds.add(400001L);
      //validIds.add(500002L);
      //dev1FenceCollection.clear();
      //dev1FenceCollection.add(NVTGeoParser.parsePolygon(factory, "0,0,0 2,1,0 0,2,0 0,0,0", 1L));
      //dev1FenceCollection1.add(NVTGeoParser.parsePolygon(factory, "2,1,0 4,0,0 4,2,0 2,1,0", 2L));
      System.out.println("end reading");
      Geometry geometry = factory.buildGeometry(dev1FenceCollection);
      start = System.currentTimeMillis();
      locator = new IndexedPointInAreaLocator(geometry, "myChange");
      locator.locate(new Coordinate(-33, -33),validIds);
      System.out.println("Tree creation time "+(System.currentTimeMillis() - start));
      //System.in.read();
      //System.out.println("Starting evaluation");
      Thread.sleep(15 * 1000);

     /* for(int j=0;j<10;j++){
        start = System.currentTimeillis();
        System.out.println(locator.locate(new Coordinate(arrX.get(j), arrY.get(j)), validIds));
        System.out.println("locate time "+(j+1)+" "+ (System.currentTimeMillis() - start));
      }
*/
     /* System.out.println("start");
      {
        File file = new File("geofence.txt");
        FileInputStream inputStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        for (int i = 100001; i <= 500000 && (line = reader.readLine()) != null; i++) {
          String arr[] = line.split("#");
          dev1FenceCollection1.add(NVTGeoParser.parsePolygon(factory, arr[1], Long.parseLong(arr[0])));
          if(i%100000 == 0){
            Geometry geometry1 = factory.buildGeometry(dev1FenceCollection1);
            IndexedPointInAreaLocator.IntervalIndexedGeometry indexedGeometry =
                new IndexedPointInAreaLocator.IntervalIndexedGeometry(geometry1,"myChange");
            start = System.currentTimeMillis();
            locator.setIndex(indexedGeometry);
            System.out.println("insertion time "+(System.currentTimeMillis() - start));
            dev1FenceCollection1.clear();
          }
        }
        line = null;
        reader.close();
        inputStream.close();
      }*/
      //dev1FenceCollection1.add(NVTGeoParser.parsePolygon(factory, "2,1,0 4,0,0 4,2,0 2,1,0", 500002L));
     dev1FenceCollection = null;
      dev1FenceCollection1 = null;
     /* for (int l=400001;l<=400501;l++){
        validIds.add(Long.parseLong(l+""));
      }*/

    /* Thread t = new Thread(new Runnable() {
        @Override
        public void run() {
          Long start = System.currentTimeMillis();

        }
      });
      t.start();*/

    locator.locate(new Coordinate(arrX.get(0), arrY.get(0)), validIds);
    start = System.currentTimeMillis();
    for (int k = 0; k < 100; k++) {
      for (int j = 0; j < 100; j++) {
        locator.locate(new Coordinate(arrX.get(j), arrY.get(j)), validIds);
        //start = System.currentTimeMillis();
      //System.out.println(locator.locate(new Coordinate(78.35515048130543,28.80459628305397), validIds).size());
//        System.out.println(
//            "locate time after insertion" + (j + 1) + " " + (System.currentTimeMillis() - start));
      }
    }
    System.out.println("locate time: " + (System.currentTimeMillis() - start));


      validIds = null;
     // Thread.sleep(10*60*1000);
    }
  }
