/*
 * The JTS Topology Suite is a collection of Java classes that
 * implement the fundamental operations required to validate a given
 * geo-spatial data set to a known topological specification.
 *
 * Copyright (C) 2001 Vivid Solutions
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, contact:
 *
 *     Vivid Solutions
 *     Suite #1A
 *     2328 Government Street
 *     Victoria BC  V8T 5G5
 *     Canada
 *
 *     (250)385-6040
 *     www.vividsolutions.com
 */
package com.vividsolutions.jts.index.intervalrtree;

import com.vividsolutions.jts.algorithm.locate.IndexedPointInAreaLocator;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.index.ItemVisitor;
import com.vividsolutions.jts.io.WKTWriter;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

public abstract class IntervalRTreeNode implements Serializable
{
	protected double min = Double.POSITIVE_INFINITY;

	protected double max = Double.NEGATIVE_INFINITY;

  protected int level =0;

  protected IntervalRTreeNode parent =null;

	protected Long polygonId;

	public Long getPolygonId() {
		return polygonId;
	}

	public double getMin() { return min; }
	public double getMax() { return max; }
	
	public abstract void query(double queryMin, double queryMax, ItemVisitor visitor);

	public abstract void query(double queryMin, double queryMax, IndexedPointInAreaLocator.SegmentVisitor visitor,
      Long parentPolygonId, Coordinate coordinate, Set<Long> validIds);

  // public abstract void insert(IntervalRTreeNode leaf, int child);

	public final boolean intersects(double queryMin, double queryMax)
	{
		if (min > queryMax 
				|| max < queryMin)
			return false;
		return true;
	}

  public void buildLevelAndMinMax(IntervalRTreeNode node){

    if (node.parent == null)
      return;

    IntervalRTreeNode c1 = ((IntervalRTreeBranchNode)node.parent).getNode1();
    IntervalRTreeNode c2 = ((IntervalRTreeBranchNode)node.parent).getNode2();
    node.parent.level = Math.max((c1 != null)?c1.level:0,(c2 != null)?c2.level:0)+1;

    node.parent.min = Math.min((c1 != null)?c1.min:0, (c2 != null)?c2.min:0);
    node.parent.max = Math.max((c1 != null)?c1.max:0, (c2 != null)?c2.max:0);

    buildLevelAndMinMax(node.parent);
  }

	public String toString()
	{
		return WKTWriter.toLineString(new Coordinate(min, 0), new Coordinate(max, 0));
	}
  
  public static class NodeComparator implements Comparator
  {
    public int compare(Object o1, Object o2)
    {
      IntervalRTreeNode n1 = (IntervalRTreeNode) o1;
      IntervalRTreeNode n2 = (IntervalRTreeNode) o2;
      double mid1 = (n1.min + n1.max) / 2;
      double mid2 = (n2.min + n2.max) / 2;
      if (mid1 < mid2) return -1;
      if (mid1 > mid2) return 1;
      return (int) (n1.polygonId - n2.polygonId);
    }
  }

}
