/*
 * The JTS Topology Suite is a collection of Java classes that
 * implement the fundamental operations required to validate a given
 * geo-spatial data set to a known topological specification.
 *
 * Copyright (C) 2001 Vivid Solutions
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, contact:
 *
 *     Vivid Solutions
 *     Suite #1A
 *     2328 Government Street
 *     Victoria BC  V8T 5G5
 *     Canada
 *
 *     (250)385-6040
 *     www.vividsolutions.com
 */
package com.vividsolutions.jts.index.intervalrtree;

import com.vividsolutions.jts.algorithm.RayCrossingCounter;
import com.vividsolutions.jts.algorithm.locate.IndexedPointInAreaLocator;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.index.*;

import java.util.Set;

public class IntervalRTreeBranchNode 
extends IntervalRTreeNode
{
	private static final int ONE = 1;
	private static final int TWO = 2;

	private final IntervalRTreeNode node1;
	private final IntervalRTreeNode node2;
	
	public IntervalRTreeBranchNode(IntervalRTreeNode n1, IntervalRTreeNode n2)
	{
		node1 = n1;
		node2 = n2;
		buildExtent(node1, node2);
	}
	
	private void buildExtent(IntervalRTreeNode n1, IntervalRTreeNode n2)
	{
		min = Math.min(n1.min, n2.min);
		max = Math.max(n1.max, n2.max);
		if (n1.getPolygonId() == n2.getPolygonId()) {
			polygonId = n1.getPolygonId();
		}
	}

	public IntervalRTreeNode getNode1() {
		return node1;
	}

	public IntervalRTreeNode getNode2() {
		return node2;
	}

	public void query(double queryMin, double queryMax, ItemVisitor visitor)
	{
		if (! intersects(queryMin, queryMax)) {
			return;
		}
		if (node1 != null) node1.query(queryMin, queryMax, visitor);
		if (node2 != null) node2.query(queryMin, queryMax, visitor);
	}

	public void query(double queryMin, double queryMax, IndexedPointInAreaLocator.SegmentVisitor visitor,
			Long parentPolygonId, Coordinate coordinate, Set<Long> validIds)
	{
		if (! intersects(queryMin, queryMax)) {
			return;
		}

		if (parentPolygonId == null && polygonId != null){
			if (!validIds.contains(polygonId)){
				return;
			}
		}

		//		System.out.println("Overlaps branch: " + this);
		if (node1 != null) node1.query(queryMin, queryMax, visitor, polygonId, coordinate, validIds);
		if (node2 != null) node2.query(queryMin, queryMax, visitor, polygonId, coordinate, validIds);
	}

/*
	@Override
	public void insert(IntervalRTreeNode leaf, int child) {
		double leafMin = leaf.getMin();
		double leafMax = leaf.getMax();
		double leafMid = (leafMin + leafMax) / 2;

		if (!intersects(leafMin, leafMax)) {
			//check for subtree to choose based on the range value
			double nodeMid = (this.min + this.max) / 2;
			if (nodeMid < leafMid) {
				if (node2 != null) {
					this.node2.insert(leaf, TWO);
				} else {
					node2 = leaf;
					node2.parent = this;
					buildLevelAndMinMax(node2);
				}
			} else {
				if (node1 != null) {
					this.node1.insert(leaf, ONE);
				} else {
					node1 = leaf;
					node1.parent = this;
					buildLevelAndMinMax(node1);
				}
			}
		} else {
			//checking the child node to traverse
			boolean c1 = (node1 != null) ? node1.intersects(leafMin, leafMax) : false;
			boolean c2 = (node2 != null) ? node2.intersects(leafMin, leafMax) : false;
			if (c1 || c2) {
				//left child contains the value
				if (c1 && !c2) {
					this.node1.insert(leaf, ONE);
				}//right child contains the value
				else if (!c1 && c2) {
					this.node2.insert(leaf, TWO);
				} //both contains the value
				else {
					//check the level, choose lower level
					if (node1.level < node2.level) {
						this.node1.insert(leaf, ONE);
					} else if (node1.level > node2.level) {
						this.node2.insert(leaf, TWO);
					} else {
						//levels are same
						double leftBucket = node1.max - node1.min;
						double rightBucket = node2.max - node2.min;
						//check for the smaller bucket to insert
						if (leftBucket < rightBucket) {
							this.node1.insert(leaf, ONE);
						} else if (leftBucket > rightBucket) {
							this.node2.insert(leaf, TWO);
						}//bucket size are same
						else {
							//check for closest one to insert
							double leftMid = (node1.max + node1.min) / 2;
							double righttMid = (node2.max + node2.min) / 2;

							if (Math.abs(leftMid - leafMid) <= Math.abs(righttMid - leafMid)) {
								this.node1.insert(leaf, ONE);
							} else {
								this.node2.insert(leaf, TWO);
							}
						}
					}
				}
			} else {
				if (node1 == null) {
					node1 = leaf;
					node1.parent = this;
					buildLevelAndMinMax(node1);
				} else if (node2 == null) {
					node2 = leaf;
					node2.parent = this;
					buildLevelAndMinMax(node2);
				} else {
					double leftMid = (node1.max + node1.min) / 2;
					double righttMid = (node2.max + node2.min) / 2;
					//check for closest one to insert
					if (Math.abs(leftMid - leafMid) < Math.abs(righttMid - leafMid)) {
						this.node1.insert(leaf, ONE);
					} else if (Math.abs(leftMid - leafMid) > Math.abs(righttMid - leafMid)) {
						this.node2.insert(leaf, TWO);
					} else {
						//both are closer at the same level
						//check the level, choose lower level
						if (node1.level <= node2.level) {
							this.node1.insert(leaf, ONE);
						} else {
							this.node2.insert(leaf, TWO);
						}

					}
				}
			}
		}
	}
*/

}
