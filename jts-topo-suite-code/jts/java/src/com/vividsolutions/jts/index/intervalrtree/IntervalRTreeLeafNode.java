/*
 * The JTS Topology Suite is a collection of Java classes that
 * implement the fundamental operations required to validate a given
 * geo-spatial data set to a known topological specification.
 *
 * Copyright (C) 2001 Vivid Solutions
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * For more information, contact:
 *
 *     Vivid Solutions
 *     Suite #1A
 *     2328 Government Street
 *     Victoria BC  V8T 5G5
 *     Canada
 *
 *     (250)385-6040
 *     www.vividsolutions.com
 */
package com.vividsolutions.jts.index.intervalrtree;

import com.vividsolutions.jts.algorithm.RayCrossingCounter;
import com.vividsolutions.jts.algorithm.locate.IndexedPointInAreaLocator;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.index.ItemVisitor;

import java.io.Serializable;
import java.util.Set;

public class IntervalRTreeLeafNode 
extends IntervalRTreeNode implements Serializable
{
  public Object getItem() {
    return item;
  }

  private Object item;

	public IntervalRTreeLeafNode(double min, double max, Object item)
	{
    this(min, max, item, null);
	}

	public IntervalRTreeLeafNode(double min, double max, Object item, Long polygonId){
    this.min = min;
    this.max = max;
    this.item = item;
    this.polygonId = polygonId;
  }

	public void query(double queryMin, double queryMax, ItemVisitor visitor)
	{
		if (! intersects(queryMin, queryMax)) 
      return;
		
		visitor.visitItem(item);
	}

  public void query(double queryMin, double queryMax, IndexedPointInAreaLocator.SegmentVisitor visitor,
      Long parentPolygonId, Coordinate c, Set<Long> validIds)
  {
    if (! intersects(queryMin, queryMax))
      return;

    if (parentPolygonId != polygonId){
      if (!validIds.contains(polygonId))
        return;
    }

    if (!visitor.getCounters().containsKey(polygonId))
      visitor.getCounters().put(polygonId, new RayCrossingCounter(c));
    visitor.visitItem(item, polygonId);
  }

/*
  @Override
  public void insert(IntervalRTreeNode leaf, int child) {
    IntervalRTreeNode node = new IntervalRTreeBranchNode((IntervalRTreeNode)this,(IntervalRTreeNode)leaf);
    node.level = Math.max(this.level,leaf.level)+1;
    if(child == 1){
      node.parent = this.parent;
      ((IntervalRTreeBranchNode)this.parent).setNode1(node);
      this.parent = node;
      leaf.parent = node;
    }else
    {
      node.parent = this.parent;
      ((IntervalRTreeBranchNode)this.parent).setNode2(node);
      this.parent = node;
      leaf.parent = node;
    }
    buildLevelAndMinMax(node);
  }
*/
}
