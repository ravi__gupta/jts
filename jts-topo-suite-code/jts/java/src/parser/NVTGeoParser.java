package parser;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by punit on 18/12/16.
 */
public class NVTGeoParser {
    public static List<Polygon> parsePolygons(GeometryFactory factory,String[] coordsStringArray, Long[] ids) {
        ArrayList<Polygon> polygons = new ArrayList<>();
        int i=0;
        for (String coordsString : coordsStringArray) {
            String[] coords = coordsString.split(" ");
            ArrayList<Coordinate> sequence = new ArrayList<>();
            for (String coord : coords) {
                String[] vals = coord.split(",");
                sequence.add(new Coordinate(Double.parseDouble(vals[0]),Double.parseDouble(vals[1]),Double.parseDouble(vals[2])));
            }
            Polygon polygon = factory.createPolygon(sequence.toArray(new Coordinate[sequence.size()]));
          polygon.setId(ids[i++]);
            polygons.add(polygon);
        }
        return polygons;
    }
    public static List<Polygon> parsePolygons(String[] coordsStringArray, Long[] ids) {
        return parsePolygons(new GeometryFactory(),coordsStringArray, ids);
    }

    public static Polygon parsePolygon(GeometryFactory factory, String PolygonVertex, Long id){
        String[] coords = PolygonVertex.split(" ");
        ArrayList<Coordinate> sequence = new ArrayList<>();
        for (String coord : coords) {
            String[] vals = coord.split(",");
            sequence.add(new Coordinate(Double.parseDouble(vals[0]),Double.parseDouble(vals[1]),Double.parseDouble(vals[2])));
        }
        Polygon polygon =  factory.createPolygon(sequence.toArray(new Coordinate[sequence.size()]));
        polygon.setId(id);
        return polygon;
    }
}
